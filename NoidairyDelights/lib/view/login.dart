import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:successivepoc/controls/common_btn.dart';
import '../helpers/constants.dart';
import 'otp_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final loginMobile = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var color = 0xff453658;
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            color: Colors.white54,
            height: MediaQuery.of(context).size.height,
            child: Center(
              child: FractionallySizedBox(
                widthFactor: 0.90,
                heightFactor: 0.85,
                child: Card(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Colors.white70,
                    ),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  elevation: 3,
                  color: Colors.white,
                  child: FractionallySizedBox(
                    widthFactor: 0.90,
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 15,
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Image.asset(
                                  NOIDAIRY_LOGO,
                                  width: 70,
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  NOIDAIRY_DELIGHTS,
                                  style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold)),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  NOIDAIRY_SLOGAN,
                                  style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600)),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height / 12,
                                ),
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Text(
                                            "Enter mobile no.",
                                            style: GoogleFonts.openSans(
                                                textStyle: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Card(
                                          elevation: 5,
                                          shadowColor: Colors.grey[100],
                                          shape: RoundedRectangleBorder(
                                            side: BorderSide(
                                              color: Colors.white70,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: TextFormField(
                                            textInputAction:
                                                TextInputAction.done,
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return INPUT_FIELD_REQUIRED;
                                              }
                                              return null;
                                            },
                                            keyboardType: TextInputType.number,
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.black),
                                            controller: loginMobile,
                                            cursorColor: Colors.black,
                                            decoration: InputDecoration(
                                                border: InputBorder.none,
                                                // labelStyle:
                                                //     GoogleFonts.openSans(
                                                //         textStyle: TextStyle(
                                                //             color: Colors.black,
                                                //             fontSize: 15,
                                                //             fontWeight:
                                                //                 FontWeight
                                                //                     .w600)),
                                                // hintText: ENTER_MOBILE_NO,
                                                contentPadding:
                                                    EdgeInsets.only(left: 20)),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      CommonButton(
                                        onButtonPressed: () {
                                          String mobileNumber =
                                              loginMobile.text;
                                          Pattern pattern =
                                              r'(^(?:[+0]9)?[0-9]{10,12}$)';
                                          RegExp regex = RegExp(pattern);
                                          print(regex.hasMatch(mobileNumber));
                                          if (regex.hasMatch(mobileNumber) ==
                                                  false ||
                                              loginMobile.text.length > 10 ||
                                              loginMobile.text.length < 10) {
                                            _scaffoldKey.currentState
                                                // ignore: deprecated_member_use
                                                .showSnackBar(SnackBar(
                                              backgroundColor: Colors.red[800],
                                              content: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    ENTER_VALID_NO,
                                                    style: GoogleFonts.openSans(
                                                        textStyle: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600)),
                                                  ),
                                                  Icon(
                                                    Icons.info_outline,
                                                    color: Colors.white,
                                                  )
                                                ],
                                              ),
                                              duration: Duration(seconds: 3),
                                            ));
                                          } else {
                                            String phoneNumber;
                                            phoneNumber = loginMobile.text;
                                            Navigator.of(context)
                                                .pushReplacement(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ValidateOTPScreen(
                                                                phoneNumber)));
                                          }
                                        },
                                        buttonText: SEND_OTP_BUTTON,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
